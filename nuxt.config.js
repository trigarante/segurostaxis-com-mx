module.exports = {
  target: "static",
  /*
   ** Headers of the page
   */
  head: {
    title: "segurodetaxi.mx",
    htmlAttrs: { lang: "es-MX" },

    meta: [
      { charset: "utf-8" },
      { hid: "robots", name: "robots", content: "index, follow" },
      { name: "viewport", content: "width=device-width, initial-scale=1" },
      { hid: "description", name: "description", content: "TAXI" },
    ],
    link: [{ rel: "icon", type: "image/png", href: "/favicon_taxi.png" }],
  },
  /*
   ** Customize the progress bar color
   */
  loading: { color: "#3bd600" },
  /*
   ** Build configuration
   */
  build: {
    //vendor: ['jquery', 'bootstrap'],
    /*
     ** Run ESLint on save
     */
    extend(config, { isDev, isClient }) {
      if (isDev && isClient) {
        config.module.rules.push({
          enforce: "pre",
          test: /\.(js|vue)$/,
          // loader: 'eslint-loader',
          exclude: /(node_modules)/,
        });
      }
    },
  },
  // include bootstrap css
  css: ["static/css/bootstrap.min.css", "~/static/css/styles.css"],
  plugins: [
    { src: "~/plugins/filters.js", ssr: false },
    { src: "~/plugins/apm-rum.js", ssr: false },
  ],
  modules: ["@nuxtjs/axios"],
  // gtm:{ id: 'GTM-5LBQ2D8' },
  // Se Modifico el baseUrl eon el dominio de proyecto actual
  env: {
    /*PRODUCCION*/
    tokenData: "MxQkZ8IhziDRxAl1PG4zvhQkYsx5XKa/mHxuu2nOXfM=",
    coreBranding: "https://dev.core-mejorseguro.com",
    catalogo: "https://dev.ws-qualitas.com",

    Environment: "DEVELOP",

    /**VALIDACIONES */
    urlValidaciones: "https://core-blacklist-service.com/rest",
    promoCore: "https://dev.core-persistance-service.com", //CORE DESCUENTOS
    hubspot: "https://core-hubspot-dev.mark-43.net/deals/landing",
  },
  render: {
    http2: { push: true },
    resourceHints: false,
    compressor: { threshold: 9 },
  },
};
