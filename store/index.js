import Vuex from 'vuex';
import getTokenService from '../plugins/getToken';
import cotizacionPromo from "~/plugins/descuentoService.js";
import telApi from "~/plugins/telefonoService.js";

const createStore = () => {
    const tokenData = process.env.tokenData
    return new Vuex.Store({
      state: {
        idEndPoint: "",
        status: "",
        message: "",
        cotizacionAli: "",
        cargandocotizacion: false,
        msj: false,
        msjEje: false,
        sin_token: false,
        tiempo_minimo: 1.3,
        config: {
          habilitarBtnEmision: false,
          aseguradora: "",
          cotizacion: true,
          emision: false,
          descuento: 0,
          telefonoAS: "88902356",
          // grupoCallback: '',
          from: "",
          idPagina: 0,
          idMedioDifusion: "",
          accessToken: "",
          ipCliente: "",
          textoSMS:
            "Ya tenemos tu cotizacion, en breve un ejecutivo te contactará.",
          desc: "",
          msi: "",
          promoLabel: "",
          promoImg: "",
          promoSpecial: false,
          extraMsg: "",
        },
        ejecutivo: {
          nombre: "",
          correo: "",
          id: 0,
        },
        formData: {
          urlOrigen: "",
          aseguradora: "",
          marca: "",
          modelo: "",
          descripcion: "",
          detalle: "",
          clave: "",
          cp: "",
          nombre: "",
          telefono: "",
          gclid_field: "",
          correo: "",
          edad: "",
          fechaNacimiento: "",
          genero: "",
          emailValid: "",
          telefonoValid: "",
          codigoPostalValid: "",
          idHubspot: "",
        },

        solicitud: {},
        cotizacion: {},
        cotizacionesAmplia: [],
        cotizacionesLimitada: [],
      },
      actions: {
        getToken() {
          return new Promise((resolve, reject) => {
            getTokenService.search(tokenData).then(
              (resp) => {
                this.state.config.accessToken = resp.data.accessToken;
                localStorage.setItem(
                  "authToken",
                  this.state.config.accessToken
                );
                resolve(resp);
              },
              (error) => {
                reject(error);
              }
            );
          });
        },
      },
      mutations: {
        cotizacionPromo: function (state) {
          cotizacionPromo
            .search(state.config.aseguradoraCatalogos)
            .then((resp) => {
              resp = resp.data;
              if (parseInt(resp.discount)) {
                state.config.descuento = resp.discount;
              } else {
                state.config.descuento = 0;
              }
              if (parseInt(resp.delay)) {
                state.config.time = resp.delay;
              } else {
                state.config.time = 5000;
              }
            })
            .catch((error) => {
              state.config.descuento = 0;
              state.config.time = 5000;
            });
        },
        telApi: function (state) {
          try {
            telApi.search(state.config.idMedioDifusion).then((resp) => {
              resp = resp.data;
              let telefono = resp.telefono;
              if (parseInt(telefono)) {
                var tel = telefono + "";
                var tel2 = tel.substring(2, tel.length);
                //   console.log(tel2);
                state.config.telefonoAS = tel2;
              }
            });
          } catch (error) {
            console.log(error);
          }
        },

        validarTokenCore: function (state) {
          try {
            if (process.browser) {
              if (localStorage.getItem("authToken") === null) {
                state.sin_token = true;
                //console.log("NO HAY TOKEN...");
              } else {
                state.config.accessToken = localStorage.getItem("authToken");
                var tokenSplit = state.config.accessToken.split(".");
                var decodeBytes = atob(tokenSplit[1]);
                var parsead = JSON.parse(decodeBytes);
                var fechaUnix = parsead.exp;
                /*
                 * Fecha formateada de unix a fecha normal
                 * */
                var expiracion = new Date(fechaUnix * 1000);
                var hoy = new Date(); //fecha actual
                /*
                 * Se obtiene el tiempo transcurrido en milisegundos
                 * */
                var tiempoTranscurrido = expiracion - hoy;
                /*
                 * Se obtienen las horas de diferencia a partir de la conversión de los
                 * milisegundos a horas.
                 * */
                var horasDiferencia =
                  Math.round(
                    (tiempoTranscurrido / 3600000 + Number.EPSILON) * 100
                  ) / 100;

                if (hoy > expiracion || horasDiferencia < state.tiempo_minimo) {
                  state.sin_token = "expirado";
                }
              }
            }
          } catch (error2) {
            // console.log(error2)
          }
        },
      },
    });
};
export default createStore
